/**
 * SPDX-PackageName: kwaeri/database-session-store
 * SPDX-PackageVersion: 1.0.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import { DatabaseSessionStore } from '../src/database-session-store.mjs';


// DEFINES
let TEST_ENV = process.env.NODE_ENV;
if( !TEST_ENV || TEST_ENV === ( "" || null ) )
    TEST_ENV = "default";


let configuration = {
        environment: TEST_ENV,
        version: "0.1.0",
        provider: "nodekit",
        type: "database",
        async: true,
        paths: {
            driver: "@kwaeri/mysql-database-driver"
        },
        table: "nodekit_sessions",
        store: {}
    },
    store = new DatabaseSessionStore( configuration ),
    sessionParams = {
        id: "0123456789",
        uk: "",
        host: "",
        domain: "",
        path: "",
        expires: 1585411327634,
        persistent: true,
        user: {
            username: "",
            email: "guest@example.com",
            name: {
                first: "Test",
                last: "User"
            }
        },
        authenticated: false
    },
    newSessionParams = {
        id: "1123456789",
        uk: "",
        host: "",
        domain: "",
        path: "",
        expires: new Date( new Date().getTime() + ( 30 * 60000 ) ).getTime(),
        persistent: true,
        user: {
            username: "",
            email: "guest@example.com",
            name: {
                first: "Test",
                last: "User"
            }
        },
        authenticated: false
    },
    newSessionParams2 = {
        id: "2123456789",
        uk: "",
        host: "",
        domain: "",
        path: "",
        expires: new Date( new Date().getTime() + ( 30 * 60000 ) ).getTime(),
        persistent: true,
        user: {
            username: "",
            email: "guest@example.com",
            name: {
                first: "Test",
                last: "User"
            }
        },
        authenticated: false
    },
    newSessionParams3 = {
        id: "3123456789",
        uk: "",
        host: "",
        domain: "",
        path: "",
        expires: new Date( new Date().getTime() + ( 30 * 60000 ) ).getTime(),
        persistent: true,
        user: {
            username: "",
            email: "guest@example.com",
            name: {
                first: "Test",
                last: "User"
            }
        },
        authenticated: false
    };


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {

        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    async () => {
                        //const version = JSON.parse( ( await fs.readFile( path.join( './', 'package.json' ), { encoding: "utf8" } ) ) ).version;

                        //console.log( `VERSION: ${version}` );

                        return Promise.resolve(
                            assert.equal( [1,2,3,4].indexOf(4), 3 )
                        );
                    }
                );

            }
        );

    }
);


// Primary tests for the module
describe(
    'Session Store Functionality Test Suite',
    () => {


        describe(
            'Create Session Test',
            () => {

                it(
                    'Should return a newly stored session found by id.',
                    async () => {
                        const returned = await store.createSession( sessionParams.id, sessionParams );

                        return Promise.resolve(
                            assert.equal( JSON.stringify( returned ), JSON.stringify( sessionParams ) )
                        );
                    }
                );

            }
        );

        describe(
            'Get Session Test',
            () => {
                it(
                    'Should return a previously stored session found by id.',
                    async () => {
                        const returned = await store.getSession( sessionParams.id );

                        return Promise.resolve(
                            assert.equal( JSON.stringify( returned ), JSON.stringify( sessionParams ) )
                        );
                    }
                );
            }
        );

        describe(
            'Set Session Parameter Test',
            () => {
                it(
                    'Should return the value of a parameter set in a stored session found by id.',
                    async () => {
                        const created             = await store.createSession( newSessionParams2.id, newSessionParams2 ),
                              returned            = await store.set( newSessionParams2.id, 'authenticated', true ),
                              deleted             = await store.deleteSession( newSessionParams2.id );

                        return Promise.resolve(
                            assert.equal( returned, true )
                        );
                    }
                );
            }
        );

        describe(
            'Get Session Parameter Test',
            () => {
                it(
                    'Should return the value of a parameter previously set in a stored session found by id.',
                    async () => {
                        const created             = await store.createSession( newSessionParams3.id, newSessionParams3 ),
                              modified            = await store.set( newSessionParams3.id, 'authenticated', true ),
                              returned            = await store.get( newSessionParams3.id, 'authenticated', true ),
                              deleted             = await store.deleteSession( newSessionParams3.id );

                        return Promise.resolve(
                            assert.equal( returned, true )
                        );
                    }
                );
            }
        );

        describe(
            'Clean Sessions Test',
            () => {
                it(
                    'Should delete any session(s) which expired before the current timestamp, causing a later direct delete to fail.',
                    async () => {
                        const returned            = await store.cleanSessions();

                        return Promise.resolve(
                            assert.equal( await store.deleteSession( sessionParams.id ), false )
                        );
                    }
                );
            }
        );

        describe(
            'Delete Sessions Test',
            () => {
                it(
                    'Should delete the specified session from the session store.',
                    async () => {
                        const created             = await store.createSession( newSessionParams.id, newSessionParams );

                        return Promise.resolve(
                            assert.equal( await store.deleteSession( newSessionParams.id ), true )
                        );
                    }
                );
            }
        );


    }
);
