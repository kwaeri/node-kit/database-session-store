/**
 * SPDX-PackageName: kwaeri/database-session-store
 * SPDX-PackageVersion: 1.0.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import {
    ClientSession,
    SessionStore,
    BaseSessionStore,
    SessionBits
} from '@kwaeri/session-store';
import { Configuration } from '@kwaeri/configuration';
import { kdt } from '@kwaeri/developer-tools';
import { DatabaseDriver, QueryResult } from  '@kwaeri/database-driver';
import debug from 'debug';


// DEFINES
const _ = new kdt(),
      DEBUG = debug( 'nodekit:database-session-store' );

export type DatabaseAPIPromise = {
    result?: boolean;
    data: any;
};

/**
 * Memcached Session Store
 *
 * Manages a 'store' object, where each memcached key is a session
 * named after its 'ID'. For each key, an object that is the session,
 * and all of its associated information, is stored within a JSON string.
 */
export class DatabaseSessionStore implements SessionStore {
    /**
     * @var { string }
     */
    public version: string;

    /**
     * @var { string }
     */
    public type: string;

    /**
     * @var { any }
     */
    protected configuration: any;

    /**
     * @var { Configuration }
     */
    private databaseConfiguration: Configuration;

    /**
     * @var { any }
     */
    private databaseConf?: any;

    /**
     * @var { string }
     */
    private table: string;

    /**
     * @var { MySQLDriver|PostgreSQLDriver }
     */
    private dbo?: DatabaseDriver

    /**
     * Class constructor
     *
     * @param { any } store The store interface. In this case, a filesystem IO interface
     * @param { any } configuration The session store configuration
     *
     * @returns { void }
     *
     * @since 0.1.1
     */
    constructor( configuration: any ) {
        // Call the parent class:
        //super( configuration );

        // Organize all of the uncertainty:
        this.version     = ( configuration && configuration.version ) ? configuration.version : null,
        this.type        = ( configuration && configuration.type ) ? configuration.type : "Database";

        // This is just the session configuration from conf/
        this.configuration = configuration;

        this.table = _.get( configuration, 'table', 'nodekit_sessions' );

        this.databaseConfiguration = new Configuration( 'conf', `database.${this.configuration.environment}.json` );
    }


    /**
     * Gets the database configuration
     *
     * @param { void }
     *
     * @returns { Promise<Configuration> } the promise for a {@link Configuration} object
     */
    private async getConf(): Promise<any> {
        if( this.databaseConf === undefined ) {
            const databaseConf = await this.databaseConfiguration.get();

            if( !databaseConf )
                    return Promise.reject( new Error( `[MYSQL_MIGRATOR][GET_CONF] There was an issue reading the database configuration. ` ) );

            DEBUG( `Database configuration successfully fetched:` );
            DEBUG( databaseConf );

            this.databaseConf = databaseConf;
        }

        // Otherwise, return the conf
        //return Promise.resolve( this[conf].configuration );  ⇦ Updated configuration implementation
        return Promise.resolve( this.databaseConf );
    }


    /**
     * Get the DBO; If dbo is undefined` then attempt to create it, and reject the
     * promise in the event that this method fails.
     *
     * @returns { Promise<DatabaseDriver> } the promise for a {@link DatabaseDriver} object
     */
    private async getDbo(): Promise<DatabaseDriver> {
        try{
            if( !this.dbo ) {
                DEBUG( `Import module '%s'`, this.configuration.paths.driver );

                const imported = await import( this.configuration.paths.driver );

                DEBUG( `Imported '%o'`, imported );

                // We're cockily expecting a single driver type of MySQLDriver
                DEBUG( `Set driver type to '%s'`, Object.keys( imported )[0] );

                const driver = imported[Object.keys( imported )[0]];
                const dbo = new driver( await this.getConf() ); //this.dbo = new Driver( await this.getConf( true ), this.databaseProvider ).get();

                if( !dbo )
                    return Promise.reject( new Error( `There was an issue getting the Database Provider` ) );

                // Ensure the database is prepped:
                const installed = await this.checkInstall( dbo );
                if( installed === null ) {
                    DEBUG( `Database table '%s' not found, install sessions table into database '%s'`, this.table, this.databaseConf?.database );

                    if( await this.installDatabaseComponent( dbo ) === undefined )
                        DEBUG( `Database table '%s' successfully installed into database '%s'`, this.table, this.databaseConf?.database );
                }

                this.dbo = dbo;
            }

            return Promise.resolve( this.dbo! );
        }
        catch( error ) {
            DEBUG( `${error}` );

            return Promise.reject( `${error}` );
        }
    }

    /**
     * Checks whether the database has been prepared for sessions
     *
     * @param { DatabaseDriver } dbo A database driver for the respective DBMS
     *
     * @returns { undefined | null } the promise for `undefined` if sessions are installed, or `null` if not
     */
    private async checkInstall( dbo: DatabaseDriver ): Promise<undefined|null> {
        // Run a query to determine if the migrations table exists::
        const checkSessionsDatabaseResult = await dbo
        //
        // information_schema queries are handled differently prior to MySQL 8;
        // If you do not capitalize the column name in the select clause, or set
        // a lowercase alias for it - you will have inconsistent use of
        // capitalization across the different MySQL versions. The simplest
        // solution is to capitalize the column name (i.e. TABLE_NAME) so that
        // the column name is capitalized in the result when querying both MySQL
        // 5 and MySQL 8 (which is the default behavior in 8, regardless of
        // select case).
        //
        // Alternatively, you can alias (i.e. TABLE_NAME as table_name) to force
        // MySQL 8 and MySQL 5 to both use lowercase for a column name:
        //
        // https://stackoverflow.com/a/54541147/2041005
        //
        .query( `SELECT TABLE_NAME FROM information_schema.tables ` +
                `WHERE table_schema = '${( this.databaseConfiguration as any ).database}' ` +
                `AND table_name = '${this.table}';` );

        DEBUG( checkSessionsDatabaseResult );
        DEBUG( checkSessionsDatabaseResult.rows );

        // Check if there was some sort of error with our query:
        if( !checkSessionsDatabaseResult || !checkSessionsDatabaseResult.rows || _.empty( checkSessionsDatabaseResult.rows ) )
            return Promise.reject( new Error( `[DATABASE_SESSION_STORE][CHECK_INSTALL] Error checking for the sessions table.` ) );

        // Check for TABLE_NAME; if the table was specified in lower case in the
        // query, but unaliased, we'd need to check for both uppercase and lower
        // here to ensure the check did not fail.
        if( ( ( checkSessionsDatabaseResult.rows as any )?.[0]?.TABLE_NAME !== this.table ) )
            return Promise.resolve( null );

        // And return undefined indicating it is indeed installed
        return Promise.resolve( undefined );
    }

    /**
     * Installs the sessions table into the database responsible for session management
     *
     * @param { DatabaseDriver } dbo A database driver for the respective DBMS
     *
     * @returns { undefined | null } the promise for `undefined` if sessions are installed successfully, or `null` if not
     */
    private async installDatabaseComponent( dbo: DatabaseDriver ): Promise<undefined> {
        // Create the sessions table:
        const createSessionsTable = await dbo
        .query( `create table if not exists ${this.table} ` +
                `( id int(11) not null auto_increment, ` +
                `sid text, ` +
                `uk text, ` +
                `host text, ` +
                `domain text, ` +
                `path text, ` +
                `persistent int(11), ` +
                `expires varchar(255), ` +
                `authenticated int(11), ` +
                `user text, ` +
                `primary key (id) );` );

        // If the query failed:
        if( !createSessionsTable || !createSessionsTable.rows )
            return Promise.reject( new Error( `[DATABASE_SESSION_STORE][INSTALL] There was an error installing sessions into the project. ` ) );

        return Promise.resolve( undefined );
    }


    /**
     * Method to read a session from the database
     *
     * @param { string } id The id of the session to use as a key value to read from
     *
     * @return { Promise<any|boolean|null> } the promise for a {@link ClientSession}, `boolean` false if not found, or `null` on error.
     */
    private async read( id: string ): Promise<any|boolean|null> {
        const dbo = await this.getDbo();

        DEBUG( `Read the session '${id}'` );

        const search = await dbo.query( `select * from ${this.table} where sid=${id};` );

        if( !search || !search.rows )
            return Promise.resolve( false );

        let session: SessionBits = {} as SessionBits;

        try {
            const data = ( search as any ).rows[0];
            session.id = ( data as any ).sid;
            session.uk = data.uk;
            session.host = data.host;
            session.domain = data.domain;
            session.path = data.path;
            session.expires = +data.expires;    // convert to number
            session.persistent = ( data.persistent ) ? true : false;
            session.user = JSON.parse( ( data as any ).user );
            session.authenticated = ( data.authenticated ) ? true: false;
        }
        catch( exception ) {
            DEBUG( `There was an issue parsing the read session user bits from the query result: %o`, exception );
            return Promise.resolve( null );
        }

        if( session && !_.empty( session ) )
            return Promise.resolve( session );
        else
            return Promise.resolve( null );
    }


    /**
     * Method to write a session to the the database
     *
     * @param { any } id The name of the session to use as a key
     * @param { SessionBits } session The session data to store with the new key
     *
     * @return { Promise<boolean|null> } the promise for a `boolean`.
     */
    private async write( id: string, session: any ): Promise<boolean> {
        const dbo = await this.getDbo();

        DEBUG( `Write session '${id}'` );

        // Write the data to the database as if it's a new record
        const written = await dbo.query(
            `insert into ${this.table} ` +
                `( \`sid\`, \`uk\`, \`host\`, \`domain\`, \`path\`, \`persistent\`, \`expires\`, \`authenticated\`, \`user\`) ` +
                `values( '${id}', '${session.uk}', '${session.host}', '${session.domain}', '${session.path}', ` +
                `${(session.persistent)?1:0}, '${session.expires}', ${(session.authenticated)?1:0}, '${JSON.stringify( session.user )}' );` );

        // If the query failed:
        if( !written || !written.rows )
            return Promise.reject( new Error( `[DATABASE_SESSION_STORE][WRITE] There was an error writing the session. ` ) );

        return Promise.resolve( true );
    }


    /**
     * Method to update a session in the database
     *
     * @param { any } id The name of the session as a key
     * @param { SessionBits } session The session data to replace for the existing key
     *
     * @return { Promise<boolean> } the promise for a `boolean`
     */
    private async update( id: string, session: SessionBits ): Promise<boolean> {
        const dbo = await this.getDbo();

        DEBUG( `Update session '${id}'` );

        // Write the data to the database as if it's a new record
        const updated = await dbo.query(
            `update ${this.table} ` +
                `set uk='${session.uk}', host='${session.host}', domain='${session.domain}', path='${session.path}', ` +
                `persistent=${(session.persistent)?1:0}, expires='${session.expires}', authenticated=${(session.authenticated)?1:0}, ` +
                `user='${JSON.stringify( session.user )}' ` +
                `where sid='${id}';` );

        // If the query failed:
        if( !updated || !updated.rows )
            return Promise.reject( new Error( `[DATABASE_SESSION_STORE][UPDATE] There was an error updating the session. ` ) );

        return Promise.resolve( true );
    }


    /**
     * Method to delete a session from the database
     *
     * @param { string } id The session id the key/value pair being deleted is named after
     *
     * @returns { Promise<boolean> } the promise for a `boolean`
     */
    private  async delete( id: string ): Promise<boolean> {
        const dbo = await this.getDbo();

        DEBUG( `Delete session '${id}'` );

        // Write the data to the database as if it's a new record
        const deleted = await dbo.query( `delete from ${this.table} where sid='${id}';` );

        // If the query failed:
        if( !deleted || !deleted.rows )
            return Promise.reject( new Error( `[DATABASE_SESSION_STORE][DELETE] There was an error deleting the session. ` ) );

        return Promise.resolve( true );
    }


    /**
     * Creates a new client session
     *
     * @param { string } id The id of the session to create
     * @param { SessionBits } clientSession The object representation of a client session
     *
     * @returns { ClientSession } the promise for a {@link ClientSession}
     */
    public async createSession( id: string, sessionBits: SessionBits = {} as SessionBits ): Promise<ClientSession> {
        DEBUG( `Create session '${id}'` );

        // When we want to create a session, first write the file:
        //this.store[id] = clientSession;
        let deferredCreate = await this.write( id, sessionBits );

        if( !deferredCreate )
            DEBUG( `There was an issue creating session '${id}': the session could not be written to the database` );

        DEBUG( `Read session '${id}' after write` );

        // Then return the read session, to ensure it was written, etc:
        let deferredRead = await this.read( id );

        //return this.store[id];
        return Promise.resolve( deferredRead );
    }


    /**
     * Returns a specific session
     *
     * @param { string } id The id of the session requested
     *
     * @returns { ClientSession } the promise for a {@link ClientSession}
     */
    public async getSession( id: string ): Promise<ClientSession|null> {
        DEBUG( `Get session '${id}'` );

        let deferredRead = await this.read( id );

        DEBUG( `Session found and read: ['%o']`, deferredRead );

        //return ( ( this.store.hasOwnProperty( id ) ) ? this.store[id] : false );
        return Promise.resolve( ( ( deferredRead ) ? deferredRead : null ) );
    };


    /**
     * Removes a specified session
     *
     * @param { string } id The id of the session to delete
     *
     * @returns { boolean } the promise for a `boolean`
     */
    public async deleteSession( id: string ): Promise<boolean> {
        let found = await this.read( id );

        if( found ) {
            DEBUG( `Delete session '${id}'` );

            let deferredDelete = await this.delete( id );

            return Promise.resolve( deferredDelete );
        }

        DEBUG( `There was an issue deleting session '${id}': the session could not be read` );

        return Promise.resolve( false );
    }


    /**
     * Gets a count of existing sessions
     *
     * @param void
     *
     * @returns { Promise<number> } the promise for a `number`
     */
    public async countSessions(): Promise<number> {
        const dbo = await this.getDbo();

        // SELECT COUNT(*) FROM count_demos;

        DEBUG( `Counting sessions` );

        // Write the data to the database as if it's a new record
        const counted = await dbo.query( `select COUNT(*) from ${this.table};` );

        // If the query failed:
        if( !counted || !counted.rows )
            return Promise.reject( new Error( `[DATABASE_SESSION_STORE][COUNT] There was an error counting the sessions. ` ) );

        return Promise.resolve( ( counted as any ).rows[0] );
    }


    /**
     * Deletes any sessions which have gone beyond their expiration
     *
     * @param void
     *
     * @returns { Promise<void> } the promise for a `void`
     */
    public async cleanSessions(): Promise<void> {
        const ts              = new Date(),
              dbo             = await this.getDbo();

        DEBUG( `Cleaning sessions` );

        // Write the data to the database as if it's a new record
        const deleted = await dbo.query( `delete from ${this.table} where expires + 0 < ${ts.getTime()};` );

        // If the query failed:
        if( !deleted || !deleted.rows )
            return Promise.reject( new Error( `[DATABASE_SESSION_STORE][DELETE] There was an error deleting the session. ` ) );

        return Promise.resolve();
    }


    /**
     * Gets the specified value from the specified session
     *
     * @param { string | number } id The id of the session the value is being requested from
     * @param { string } name The name of the session value that is being requested
     * @param { any } defaultValue A default value in the event the property being requested is unknown or unset
     *
     * @returns { any }
     */
    public async get( id: string, name: string, defaultValue: any ): Promise<any> {
        DEBUG( `Get '%s' from session '%s'`, name, id );

        const deferredRead = await this.read( id );

        const returnable = _.get( deferredRead, name, defaultValue );

        DEBUG( `Got '%s:%s' from '%s'`, name, returnable, id );

        if( !deferredRead ) {
            DEBUG( `Error getting '${name}' from session '${id}'` );

            Promise.reject( new Error( `[DATABASE_SESSION_STORE] When getting '${name}', the session '${id}' could not be read. ` ) );
        }

        return Promise.resolve( returnable );
    }


    /**
     * Sets the specified value of the specified session
     *
     * @param { string | number } id The id of the session for which the value is being set
     * @param { string } name The name of the property for the specified session for which the value is being set
     * @param { any } value The value being set for the specified property of the specified session
     *
     * @returns { any }
     */
    public async set( id: string, name: string, value: any ): Promise<any> {
        DEBUG( `Set '${name}' to '${value}' for session '${id}'` );

        const deferredRead = await this.read( id );

        if( !deferredRead ) {
            DEBUG( `Error setting '${name}' to '${value}' for session '${id}'` );

            Promise.reject( new Error( `[DATABASE_SESSION_STORE] When setting '${name}' to '${value}', the session '${id}' could not be found. ` ) );
        }

        const session = _.set( deferredRead, name, value );

        const deferredUpdate = await this.update( id, session );

        if( !deferredUpdate ) {
            DEBUG( `Error setting '${name}' to '${value}', for session '${id}': session could not be stored with the database server` );

            Promise.reject( new Error( `When setting '${name}' to '${value}', the session '${id}' could not be stored with the database. ` ) );
        }

        return Promise.resolve( session[name] );
    }
}